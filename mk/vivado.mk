# Copyright 2021 Raphaël Bresson

build/vivado:
	@echo "----------------------------------------------------------------"
	@echo "--- VIVADO BUILD DIRECTORIES CREATION AND SCRIPTS GENERATION ---"
	@echo "----------------------------------------------------------------"
	@echo "### INFO: Creating directory ${PWD}/$@/build"
	@mkdir -p $@/build
	@echo "### INFO: Creating directory ${PWD}/$@/synth_out"
	@mkdir -p $@/synth_out
	@echo "### INFO: Creating directory ${PWD}/$@/opt_out"
	@mkdir -p $@/opt_out
	@echo "### INFO: Creating directory ${PWD}/$@/placement_out"
	@mkdir -p $@/placement_out
	@echo "### INFO: Creating directory ${PWD}/$@/route_out"
	@mkdir -p $@/route_out
	@echo "### INFO: Creating directory ${PWD}/$@/bitstream_out"
	@mkdir -p $@/bitstream_out
	@echo "### INFO: Creating directory ${PWD}/$@/dcp"
	@mkdir -p $@/dcp
	@echo "### INFO: Creating directory ${PWD}/$@/script"
	@mkdir -p $@/script

build/vivado/script/import_pkg_vhd.tcl: ${SYNTH_PKG_VHD_FILES} | build/vivado
	@echo "### INFO: Generating tcl script ${PWD}/$@ and bash script ${PWD}/build/vivado/script/save_pkg_vhd.sh"
	@rm -f $@
	@touch $@
	@rm -f build/vivado/script/save_pkg_vhd.sh
	@touch build/vivado/script/save_pkg_vhd.sh
	@for f in `find rtl/synth -name *.vhd | grep "pkg"`; do \
	  cp $${f} build/vivado/build/; \
		echo "read_vhdl build/vivado/build/`basename $${f}`" >> $@; \
		echo "cp build/vivado/build/`basename $${f}` $${f}" >> build/vivado/script/save_pkg_vhd.sh; \
	done

build/vivado/script/import_pkg_vhdl.tcl: ${SYNTH_PKG_VHDL_FILES} | build/vivado
	@echo "### INFO: Generating tcl script ${PWD}/$@ and bash script ${PWD}/build/vivado/script/save_pkg_vhdl.sh"
	@rm -f $@
	@touch $@
	@rm -f build/vivado/script/save_pkg_vhdl.sh
	@touch build/vivado/script/save_pkg_vhdl.sh
	@for f in `find rtl/synth -name *.vhdl | grep "pkg"`; do \
		cp $${f} build/vivado/build/; \
		echo "read_vhdl build/vivado/build/`basename $${f}`" >> $@; \
		echo "cp build/vivado/build/`basename $${f}` $${f}" >> build/vivado/script/save_pkg_vhdl.sh; \
	done

build/vivado/script/import_pkg_sv.tcl: ${SYNTH_PKG_SV_FILES} | build/vivado
	@echo "### INFO: Generating tcl script ${PWD}/$@ and bash script ${PWD}/build/vivado/script/save_pkg_sv.sh"
	@rm -f $@
	@touch $@
	@rm -f build/vivado/script/save_pkg_sv.sh
	@touch build/vivado/script/save_pkg_sv.sh
	@for f in `find rtl/synth -name *.sv | grep "pkg"`; do \
		cp $${f} build/vivado/build/; \
		echo "read_verilog -sv build/vivado/build/`basename $${f}`" >> $@; \
		echo "cp build/vivado/build/`basename $${f}` $${f}" >> build/vivado/script/save_pkg_sv.sh; \
	done

build/vivado/script/import_pkg.tcl: build/vivado/script/import_pkg_sv.tcl build/vivado/script/import_pkg_vhdl.tcl build/vivado/script/import_pkg_vhd.tcl | build/vivado
	@echo "### INFO: Generating tcl script ${PWD}/$@"
	@echo "source build/vivado/script/import_pkg_sv.tcl"    > $@
	@echo "source build/vivado/script/import_pkg_vhd.tcl"  >> $@
	@echo "source build/vivado/script/import_pkg_vhdl.tcl" >> $@

build/vivado/script/import_pre_synth.tcl: ${CONSTR_PRE_SYNTH} | build/vivado
	@echo "### INFO: Generating tcl script ${PWD}/$@"
	@rm -f $@
	@touch $@
	@for f in `find rtl/pre_synth -name *.tcl`; do \
	  cp $${f} build/vivado/build/; \
		echo "source build/vivado/build/`basename $${f}`" >> $@; \
	done
	@for f in `find rtl/pre_synth -name *.xdc`; do \
	  cp $${f} build/vivado/build/; \
		echo "read_xdc build/vivado/build/`basename $${f}`" >> $@; \
	done

build/vivado/script/import_post_synth.tcl: ${CONSTR_POST_SYNTH} | build/vivado
	@echo "### INFO: Generating tcl script ${PWD}/$@"
	@rm -f $@
	@touch $@
	@for f in `find rtl/post_synth -name *.tcl`; do \
	  cp $${f} build/vivado/build/; \
		echo "source build/vivado/build/`basename $${f}`" >> $@; \
	done
	@for f in `find rtl/post_synth -name *.xdc`; do \
	  cp $${f} build/vivado/build/; \
		echo "read_xdc build/vivado/build/`basename $${f}`" >> $@; \
	done

build/vivado/script/import_pre_opt.tcl: ${CONSTR_PRE_OPT} ${CONSTR_PROBES} | build/vivado
	@echo "### INFO: Generating tcl script ${PWD}/$@"
	@rm -f $@
	@touch $@
	@for f in `find rtl/pre_opt -name *.tcl`; do \
	  cp $${f} build/vivado/build/; \
		echo "source build/vivado/build/`basename $${f}`" >> $@; \
	done
	@for f in `find rtl/pre_opt -name *.xdc`; do \
	  cp $${f} build/vivado/build/; \
		echo "read_xdc build/vivado/build/`basename $${f}`" >> $@; \
	done
	@if [ "${USE_PROBES}" == "YES" ]; then \
		for f in `find rtl/probes -name *.tcl`; do \
		  cp $${f} build/vivado/build/; \
			echo "source build/vivado/build/`basename $${f}`" >> $@; \
		done; \
		for f in `find rtl/probes -name *.xdc`; do \
		  cp $${f} build/vivado/build/; \
			echo "read_xdc build/vivado/build/`basename $${f}`" >> $@; \
		done; \
	fi;

build/vivado/script/import_post_opt.tcl: ${CONSTR_POST_OPT} | build/vivado
	@echo "### INFO: Generating tcl script ${PWD}/$@"
	@rm -f $@
	@touch $@
	@for f in `find rtl/post_opt -name *.tcl`; do \
	  cp $${f} build/vivado/build/; \
		echo "source build/vivado/build/`basename $${f}`" >> $@; \
	done
	@for f in `find rtl/post_opt -name *.xdc`; do \
	  cp $${f} build/vivado/build/; \
		echo "read_xdc build/vivado/build/`basename $${f}`" >> $@; \
	done

build/vivado/script/import_pre_placement.tcl: ${CONSTR_PRE_PLACEMENT} | build/vivado
	@echo "### INFO: Generating tcl script ${PWD}/$@"
	@rm -f $@
	@touch $@
	@for f in `find rtl/pre_placement -name *.tcl`; do \
	  cp $${f} build/vivado/build/; \
		echo "source build/vivado/build/`basename $${f}`" >> $@; \
	done
	@for f in `find rtl/pre_placement -name *.xdc`; do \
	  cp $${f} build/vivado/build/; \
		echo "read_xdc build/vivado/build/`basename $${f}`" >> $@; \
	done

build/vivado/script/import_post_placement.tcl: ${CONSTR_POST_PLACEMENT} | build/vivado
	@echo "### INFO: Generating tcl script ${PWD}/$@"
	@rm -f $@
	@touch $@
	@for f in `find rtl/post_placement -name *.tcl`; do \
	  cp $${f} build/vivado/build/; \
		echo "source build/vivado/build/`basename $${f}`" >> $@; \
	done
	@for f in `find rtl/post_placement -name *.xdc`; do \
	  cp $${f} build/vivado/build/; \
		echo "read_xdc build/vivado/build/`basename $${f}`" >> $@; \
	done

build/vivado/script/import_pre_route.tcl: ${CONSTR_PRE_ROUTE} | build/vivado
	@echo "### INFO: Generating tcl script ${PWD}/$@"
	@rm -f $@
	@touch $@
	@for f in `find rtl/pre_route -name *.tcl`; do \
	  cp $${f} build/vivado/build/; \
		echo "source build/vivado/build/`basename $${f}`" >> $@; \
	done
	@for f in `find rtl/pre_route -name *.xdc`; do \
	  cp $${f} build/vivado/build/; \
		echo "read_xdc build/vivado/build/`basename $${f}`" >> $@; \
	done

build/vivado/script/import_post_route.tcl: ${CONSTR_POST_ROUTE} | build/vivado
	@echo "### INFO: Generating tcl script ${PWD}/$@"
	@rm -f $@
	@touch $@
	@for f in `find rtl/post_route -name *.tcl`; do \
	  cp $${f} build/vivado/build/; \
		echo "source build/vivado/build/`basename $${f}`" >> $@; \
	done
	@for f in `find rtl/post_route -name *.xdc`; do \
	  cp $${f} build/vivado/build/; \
		echo "read_xdc build/vivado/build/`basename $${f}`" >> $@; \
	done

build/vivado/script/import_pre_bitstream.tcl: ${CONSTR_PRE_BITSTREAM} | build/vivado
	@echo "### INFO: Generating tcl script ${PWD}/$@"
	@rm -f $@
	@touch $@
	@for f in `find rtl/pre_bitstream -name *.tcl`; do \
	  cp $${f} build/vivado/build/; \
		echo "source build/vivado/build/`basename $${f}`" >> $@; \
	done
	@for f in `find rtl/pre_bitstream -name *.xdc`; do \
	  cp $${f} build/vivado/build/; \
		echo "read_xdc build/vivado/build/`basename $${f}`" >> $@; \
	done

build/vivado/script/import_vhd.tcl: ${SYNTH_VHD_FILES} | build/vivado
	@echo "### INFO: Generating tcl script ${PWD}/$@ and bash script ${PWD}/build/vivado/script/save_vhd.sh"
	@rm -f $@
	@touch $@
	@rm -f build/vivado/script/save_vhd.sh
	@touch build/vivado/script/save_vhd.sh
	@for f in `find rtl/synth -name *.vhd | grep -v "pkg"`; do \
	  cp $${f} build/vivado/build/; \
		echo "read_vhdl build/vivado/build/`basename $${f}`" >> $@; \
		echo "cp build/vivado/build/`basename $${f}` $${f}" >> build/vivado/script/save_vhd.sh; \
	done

build/vivado/script/import_vhdl.tcl: ${SYNTH_VHDL_FILES} | build/vivado
	@echo "### INFO: Generating tcl script ${PWD}/$@ and bash script ${PWD}/build/vivado/script/save_vhd.sh"
	@rm -f $@
	@touch $@
	@rm -f build/vivado/script/save_vhdl.sh
	@touch build/vivado/script/save_vhdl.sh
	@for f in `find rtl/synth -name *.vhdl | grep -v "pkg"`; do \
		cp $${f} build/vivado/build/; \
		echo "read_vhdl build/vivado/build/`basename $${f}`" >> $@; \
		echo "cp build/vivado/build/`basename $${f}` $${f}" >> build/vivado/script/save_vhdl.sh; \
	done

build/vivado/script/import_verilog.tcl: ${SYNTH_V_FILES} | build/vivado
	@echo "### INFO: Generating tcl script ${PWD}/$@ and bash script ${PWD}/build/vivado/script/save_verilog.sh"
	@rm -f $@
	@touch $@
	@rm -f build/vivado/script/save_verilog.sh
	@touch build/vivado/script/save_verilog.sh
	@for f in `find rtl/synth -name *.v`; do \
		cp $${f} build/vivado/build/; \
		echo "read_verilog build/vivado/build/`basename $${f}`" >> $@; \
		echo "cp build/vivado/build/`basename $${f}` $${f}" >> build/vivado/script/save_verilog.sh; \
	done

build/vivado/script/import_system_verilog.tcl: ${SYNTH_SV_FILES} | build/vivado
	@echo "### INFO: Generating tcl script ${PWD}/$@ and bash script ${PWD}/build/vivado/script/save_system_verilog.sh"
	@rm -f $@
	@touch $@
	@rm -f build/vivado/script/save_system_verilog.sh
	@touch build/vivado/script/save_system_verilog.sh
	@for f in `find rtl/synth -name *.sv | grep -v "pkg"`; do \
		cp $${f} build/vivado/build; \
		echo "read_verilog -sv build/vivado/build/`basename $${f}`" >> $@; \
		echo "cp build/vivado/build/`basename $${f}` $${f}" >> build/vivado/script/save_system_verilog.sh; \
	done

build/vivado/script/import_ips.tcl: ${SYNTH_XCI_FILES} | build/vivado
	@echo "### INFO: Generating tcl script ${PWD}/$@ and bash script ${PWD}/build/vivado/script/save_ips.sh"
	@rm -f $@
	@touch $@
	@rm -f build/vivado/script/save_ips.sh
	@touch build/vivado/script/save_ips.sh
	@for f in `find rtl/synth -name *.xci`; do \
		cp $${f} build/vivado/build; \
		echo "read_ip build/vivado/build/`basename $${f}`" >> $@; \
		echo "set_property part ${PART} [current_project]" >> $@; \
		echo "set_property board_part ${BOARD} [current_project]" >> $@; \
		echo "set_property target_language ${RTL_LANGUAGE} [current_project]" >> $@; \
		echo "generate_target all [get_files build/vivado/`basename $${f}`] -force" >> $@; \
		echo "export_ip_user_files -of_objects [get_files build/vivado/build/`basename $${f}`] -no_script -force" >> $@; \
		echo "update_ip_catalog" >> $@; \
		echo "cp build/vivado/build/`basename $${f}` $${f}" >> build/vivado/script/save_ips.sh; \
	done

build/vivado/script/import_bds.tcl: ${SYNTH_BD_FILES} | build/vivado
	@echo "### INFO: Generating tcl script ${PWD}/$@ and bash script ${PWD}/build/vivado/script/save_bds.sh"
	@rm -f $@
	@touch $@
	@rm -f build/vivado/script/save_bds.sh
	@touch build/vivado/script/save_bds.sh
	@for f in `find rtl/synth -name *.bd`; do \
		cp $${f} build/vivado/build; \
		echo "read_bd build/vivado/build/`basename $${f}`" >> $@; \
		echo "set_property part ${PART} [current_project]" >> $@; \
		echo "set_property board_part ${BOARD} [current_project]" >> $@; \
		echo "set_property target_language ${RTL_LANGUAGE} [current_project]" >> $@; \
		echo "generate_target all [get_files build/vivado/build/`basename $${f}`] -force" >> $@; \
		echo "export_ip_user_files -of_objects [get_files build/vivado/build/`basename $${f}`] -no_script -force" >> $@; \
		echo "make_wrapper -files [get_files build/vivado/build/`basename $${f}`] -top" >> $@; \
		if [[ "${RTL_LANGUAGE}" == "VHDL" ]]; then \
			echo "read_vhdl build/vivado/build/hdl/$$(basename $${f%.*})_wrapper.vhd" >> $@; \
		else \
			echo "read_verilog build/vivado/build/hdl/$$(basename $${f%.*})_wrapper.v" >> $@; \
		fi; \
		echo "cp build/vivado/build/`basename $${f}` $${f}" >> build/vivado/script/save_bds.sh; \
	done

build/vivado/script/import_synth.tcl: build/vivado/script/import_pre_synth.tcl build/vivado/script/import_vhd.tcl build/vivado/script/import_vhdl.tcl build/vivado/script/import_verilog.tcl build/vivado/script/import_system_verilog.tcl build/vivado/script/import_bds.tcl build/vivado/script/import_ips.tcl build/vivado/script/import_pkg.tcl | build/vivado
	@echo "### INFO: Generating tcl script ${PWD}/$@"
	@echo "source build/vivado/script/import_pkg.tcl"             > $@
	@echo "source build/vivado/script/import_vhd.tcl"            >> $@
	@echo "source build/vivado/script/import_vhdl.tcl"           >> $@
	@echo "source build/vivado/script/import_verilog.tcl"        >> $@
	@echo "source build/vivado/script/import_system_verilog.tcl" >> $@
	@echo "source build/vivado/script/import_ips.tcl"            >> $@
	@echo "source build/vivado/script/import_bds.tcl"            >> $@
	@echo "source build/vivado/script/import_pre_synth.tcl"      >> $@

build/vivado/script/save_synth_files.sh: build/vivado/script/import_synth.tcl | build/vivado
	@echo "### INFO: Generating bash script ${PWD}/$@"
	@echo "bash build/vivado/script/save_vhd.sh"             > $@
	@echo "bash build/vivado/script/save_vhdl.sh"           >> $@
	@echo "bash build/vivado/script/save_verilog.sh"        >> $@
	@echo "bash build/vivado/script/save_system_verilog.sh" >> $@
	@echo "bash build/vivado/script/save_ips.sh"            >> $@
	@echo "bash build/vivado/script/save_bds.sh"            >> $@

build/vivado/script/synth.tcl: build/vivado/script/import_synth.tcl build/vivado/script/import_post_synth.tcl | build/vivado
	@echo "### INFO: Generating tcl script ${PWD}/$@"
	@echo "source build/vivado/script/import_synth.tcl"                                > $@
	@echo "synth_design -top ${TOP}  -part ${PART} -directive ${SYNTHESIS_DIRECTIVE}" >> $@
	@echo "source build/vivado/script/import_post_synth.tcl"                                 >> $@
	@echo "report_timing_summary     -file build/vivado/synth_out/timing.rpt"         >> $@
	@echo "report_power              -file build/vivado/synth_out/power.rpt"          >> $@
	@echo "report_clock_utilization  -file build/vivado/synth_out/clock_util.rpt"     >> $@
	@echo "report_utilization        -file build/vivado/synth_out/utilisation.rpt"    >> $@
	@echo "report_drc                -file build/vivado/synth_out/drc.rpt"            >> $@
	@echo "write_verilog -force      -file build/vivado/synth_out/netlist.v"          >> $@
	@echo "write_xdc -no_fixed_only -force build/vivado/synth_out/bft.xdc"            >> $@
	@echo "write_checkpoint         -force build/vivado/dcp/synth.dcp"                >> $@

build/vivado/script/opt.tcl: build/vivado/script/synth.tcl build/vivado/script/import_pre_opt.tcl build/vivado/script/import_post_opt.tcl | build/vivado
	@echo "### INFO: Generating tcl script ${PWD}/$@"
	@echo "set_property part ${PART} [current_project]"                           > $@
	@echo "set_property board_part ${BOARD} [current_project]"                   >> $@
	@echo "source build/vivado/script/import_pre_opt.tcl"                        >> $@
	@echo "opt_design -directive ${OPT_DIRECTIVE}"                               >> $@
	@echo "source build/vivado/script/import_post_opt.tcl"                       >> $@
	@echo "report_timing_summary     -file build/vivado/opt_out/timing.rpt"      >> $@
	@echo "report_clock_utilization  -file build/vivado/opt_out/clock_util.rpt"  >> $@
	@echo "report_utilization        -file build/vivado/opt_out/utilisation.rpt" >> $@
	@echo "report_power              -file build/vivado/opt_out/power.rpt"       >> $@
	@echo "report_drc                -file build/vivado/opt_out/drc.rpt"         >> $@
	@echo "write_verilog -force            build/vivado/opt_out/netlist.v"       >> $@
	@echo "write_xdc -no_fixed_only -force build/vivado/opt_out/bft.xdc"         >> $@
	@echo "write_checkpoint         -force build/vivado/dcp/opt.dcp"             >> $@

build/vivado/script/placement.tcl: build/vivado/script/opt.tcl build/vivado/script/import_pre_placement.tcl build/vivado/script/import_post_placement.tcl | build/vivado
	@echo "### INFO: Generating tcl script ${PWD}/$@"
	@echo "source build/vivado/script/import_pre_placement.tcl"                         > $@
	@echo "place_design -directive ${PLACEMENT_DIRECTIVE}"                             >> $@
	@echo "source build/vivado/script/import_post_placement.tcl"                       >> $@
	@echo "report_timing_summary     -file build/vivado/placement_out/timing.rpt"      >> $@
	@echo "report_clock_utilization  -file build/vivado/placement_out/clock_util.rpt"  >> $@
	@echo "report_utilization        -file build/vivado/placement_out/utilisation.rpt" >> $@
	@echo "report_power              -file build/vivado/placement_out/power.rpt"       >> $@
	@echo "report_drc                -file build/vivado/placement_out/drc.rpt"         >> $@
	@echo "write_verilog            -force build/vivado/placement_out/netlist.v"       >> $@
	@echo "write_xdc -no_fixed_only -force build/vivado/placement_out/bft.xdc"         >> $@
	@echo "write_checkpoint         -force build/vivado/dcp/placement.dcp"             >> $@

build/vivado/script/routing.tcl: build/vivado/script/placement.tcl build/vivado/script/import_pre_route.tcl build/vivado/script/import_post_route.tcl | build/vivado
	@echo "### INFO: Generating tcl script ${PWD}/$@"
	@echo "source build/vivado/script/import_pre_route.tcl"                         > $@
	@echo "route_design -directive ${ROUTE_DIRECTIVE}"                             >> $@
	@echo "source build/vivado/script/import_post_route.tcl"                       >> $@
	@echo "report_timing_summary     -file build/vivado/route_out/timing.rpt"      >> $@
	@echo "report_clock_utilization  -file build/vivado/route_out/clock_util.rpt"  >> $@
	@echo "report_utilization        -file build/vivado/route_out/utilisation.rpt" >> $@
	@echo "report_power              -file build/vivado/route_out/power.rpt"       >> $@
	@echo "report_drc                -file build/vivado/route_out/drc.rpt"         >> $@
	@echo "write_verilog            -force build/vivado/route_out/netlist.v"       >> $@
	@echo "write_xdc -no_fixed_only -force build/vivado/route_out/bft.xdc"         >> $@
	@echo "write_checkpoint         -force build/vivado/dcp/routing.dcp"           >> $@

build/vivado/script/bitstream.tcl: build/vivado/script/routing.tcl build/vivado/script/import_pre_bitstream.tcl | build/vivado
	@echo "### INFO: Generating tcl script ${PWD}/$@"
	@echo "source build/vivado/script/import_pre_bitstream.tcl"                         > $@
	@echo "write_bitstream -force          build/vivado/bitstream.bit"                 >> $@
	@echo "write_hwdef -force              build/vivado/system.hdf"                    >> $@
	@echo "report_timing_summary     -file build/vivado/bitstream_out/timing.rpt"      >> $@
	@echo "report_clock_utilization  -file build/vivado/bitstream_out/clock_util.rpt"  >> $@
	@echo "report_utilization        -file build/vivado/bitstream_out/utilisation.rpt" >> $@
	@echo "report_power              -file build/vivado/bitstream_out/power.rpt"       >> $@
	@echo "report_drc                -file build/vivado/bitstream_out/drc.rpt"         >> $@
	@echo "write_verilog -force            build/vivado/bitstream_out/netlist.v"       >> $@
	@echo "write_xdc -no_fixed_only -force build/vivado/bitstream_out/bft.xdc"         >> $@
	@echo "write_checkpoint         -force build/vivado/dcp/bitstream.dcp"             >> $@
	@if [ "${USE_PROBES}" == "YES" ]; then \
		echo "write_debug_probes build/vivado/probes.ltx" >> $@; \
	fi;

build/vivado/import-synth.done: build/vivado/script/import_synth.tcl
	@echo "----------------------------------------------------------------"
	@echo "---              SYNTHETIZABLE FILES IMPORTATION             ---"
	@echo "----------------------------------------------------------------"
	@rm -rf build/vivado/sim
	@vivado -mode batch -source build/vivado/script/import_synth.tcl -nojournal -nolog
	@echo "DONE" > build/vivado/import-synth.done

build/vivado/dcp/synth.dcp: build/vivado/script/synth.tcl
	@echo "----------------------------------------------------------------"
	@echo "---                         SYNTHESIS                        ---"
	@echo "----------------------------------------------------------------"
	@rm -rf build/vivado/sim
	@rm -rf build/xsct
	@echo "### INFO: Launching synthesis with top level module: ${TOP}"
	@vivado -mode batch -source build/vivado/script/synth.tcl -nojournal -log build/vivado/synth_out/synth.log
	@echo "### INFO: Synthesis terminated succesfully with top level module: ${TOP}"
	@echo "DONE" > build/vivado/import-synth.done

build/vivado/dcp/opt.dcp: build/vivado/script/bitstream.tcl build/vivado/dcp/synth.dcp
	@echo "----------------------------------------------------------------"
	@echo "---                        OPT DESIGN                        ---"
	@echo "----------------------------------------------------------------"
	@echo "### INFO: Launching OPT DESIGN for top level module: ${TOP}"
	@vivado -mode batch -source build/vivado/script/opt.tcl -nojournal build/vivado/dcp/synth.dcp -log build/vivado/opt_out/opt.log
	@echo "### INFO: OPT DESIGN step successfully finished for top level module: ${TOP}"

build/vivado/dcp/placement.dcp: build/vivado/dcp/opt.dcp
	@echo "----------------------------------------------------------------"
	@echo "---                         PLACEMENT                        ---"
	@echo "----------------------------------------------------------------"
	@echo "### INFO: Launching PLACEMENT for top level module: ${TOP}"
	@vivado -mode batch -source build/vivado/script/placement.tcl -nojournal build/vivado/dcp/opt.dcp -log build/vivado/placement_out/placement.log
	@echo "### INFO: PLACEMENT step successfully finished for top level module: ${TOP}"

build/vivado/dcp/routing.dcp: build/vivado/dcp/placement.dcp
	@echo "----------------------------------------------------------------"
	@echo "---                           ROUTE                          ---"
	@echo "----------------------------------------------------------------"
	@echo "### INFO: Launching ROUTING for top level module: ${TOP}"
	@vivado -mode batch -source build/vivado/script/routing.tcl -nojournal build/vivado/dcp/placement.dcp -log build/vivado/route_out/routing.log
	@echo "### INFO: ROUTING step successfully finished for top level module: ${TOP}"

build/vivado/system.hdf: build/vivado/dcp/routing.dcp
	@echo "----------------------------------------------------------------"
	@echo "---                   BITSTREAM GENERATION                   ---"
	@echo "----------------------------------------------------------------"
	@echo "### INFO: Generating bitstream and hardware definition file for top level module: ${TOP}"
	@vivado -mode batch -source build/vivado/script/bitstream.tcl -nojournal build/vivado/dcp/routing.dcp -log build/vivado/bitstream_out/bitstream.log
	@cp build/vivado/bitstream.bit os/board/${BR2_BOARD}/fpga.bit
	@echo "### INFO: Bitstream and hardware definition file successfully created for top level module: ${TOP}"

vivado-all: build/vivado/system.hdf

vivado-gui: build/vivado/script/save_synth_files.sh
	@echo "### INFO: Opening Vivado in gui mode"
	@vivado -nojournal -nolog -source build/vivado/script/import_synth.tcl
	@echo "Would you like to save the modified files (from ${PWD}/build/vivado/build to ${PWD}/rtl/synth)? [y, N]"
	@read rc; \
	if [[ "$${rc}" == @(y|Y) ]]; then \
		echo "### INFO: Copying files from ${PWD}/build/vivado/build to ${PWD}/rtl/synth"; \
		bash build/vivado/script/save_synth_files.sh; \
	else \
		echo "### INFO: Modified files from ${PWD}/build/vivado/build not saved"; \
		echo "### INFO: Modified files will be automatically restored on the next invocation of vivado"; \
		rm -f build/vivado/script/import_*; \
	fi
	@echo "DONE" > build/vivado/import-synth.done

vivado-gui-synth: build/vivado/dcp/synth.dcp
	@echo "### INFO: Opening Vivado synthetized design in gui mode"
	@vivado -nojournal -nolog build/vivado/dcp/synth.dcp

vivado-gui-opt: build/vivado/dcp/opt.dcp
	@echo "### INFO: Opening Vivado opt design in gui mode"
	@vivado -nojournal -nolog build/vivado/dcp/opt.dcp

vivado-gui-placement: build/vivado/dcp/placement.dcp
	@echo "### INFO: Opening Vivado placed design in gui mode"
	@vivado -nojournal -nolog build/vivado/dcp/placement.dcp

vivado-gui-route: build/vivado/dcp/routing.dcp
	@echo "### INFO: Opening Vivado routed design in gui mode"
	@vivado -nojournal -nolog build/vivado/dcp/routing.dcp

vivado-clean:
	@echo "### INFO: Cleaning vivado outputs"
	@rm -rf build/vivado
